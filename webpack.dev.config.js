//Imports
var webpack = require('webpack');
//Webpack plugins
var HtmlWebpackPlugin = require('html-webpack-plugin');
//Common webpack configuration
var commonConfig = require('./webpack.common.config');

var config = {
    entry: {
        app: commonConfig.appEntries.concat([
            'webpack/hot/dev-server',
            'webpack-dev-server/client?http://localhost:8080'
        ]),
        vendors: commonConfig.vendors
    },
    output: {
        path: commonConfig.outputPath,
        filename: 'bundle.js'
    },
    resolve: commonConfig.resolve,
    plugins: [
        new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js'),
        new HtmlWebpackPlugin({
            title: 'Tuerking',
            template: 'src/index.ejs',
            reload: 'http://localhost:8080/webpack-dev-server'
        })
    ],
    devtool: 'source-map',
    noInfo:true,
    module: {
        loaders: commonConfig.loaders
    }
};

module.exports = config;