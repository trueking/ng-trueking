//Webpack common builds options
'use strict';

var webpack = require('webpack');
var path = require("path");

var _appEntries = [path.resolve(__dirname, 'src/main.ts')];
var _outputPath = path.resolve(__dirname, "build");

var _vendors = [
    //js
    'angular',
    //Angular modules
    'angular-ui-router',
    //Styles
    'bootstrap/dist/css/bootstrap.css',
    'font-awesome/css/font-awesome.css'
];

var _resolve = {
    extensions: ['', '.ts', '.js', '.css'],
    modulesDirectories: ['node_modules']
};

var _loaders = [
    {test: /\.ts$/, loader: 'ts-loader'},
    {test: /\.css$/, loader: 'style!css'},
    {test: /\.html$/, loader: 'ngtemplate?relativeTo=' + (path.resolve(__dirname, './src')) + '/!html'},
    {test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader?limit=10000&mimetype=application/font-woff'},
    {test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'file-loader'},
    {test: '\.jpg$', exclude: /node_modules/, loader: 'file'},
    {test: '\.png$', exclude: /node_modules/, loader: 'url'}
];

module.exports = {
    appEntries: _appEntries,
    outputPath: _outputPath,
    vendors: _vendors,
    resolve: _resolve,
    loaders: _loaders
};

