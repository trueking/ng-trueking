'use strict';

var webpackCommonConfig = require('../webpack.common.config');

var _basePath = '../';

var _files = [
    //Global vendors (default imports)
    'node_modules/angular/angular.js',
    'node_modules/angular-mocks/angular-mocks.js',
    'node_modules/angular-ui-route/angular-ui-route/angular-ui-route.js',
    //My tests
    'test/unit/**/*.unit.ts'
];

var _webpack = {
    noInfo: true,
    resolve: webpackCommonConfig.resolve,
    module: {
        loaders: webpackCommonConfig.loaders
    }
};

var _webpackMiddleware = {
    noInfo: true,
    stats: {
        color: true,
        chunkModules: false,
        modules: false
    }
};


module.exports = {
    basePath: _basePath,
    files: _files,
    webpack: _webpack,
    webpackMiddleware: _webpackMiddleware
};

