'use strict';

var commonConfig = require('./karma.common.conf');

module.exports = function (config) {
    config.set({

        basePath: commonConfig.basePath,

        preprocessors: {
            'test/**/*.unit.ts': ['webpack']
        },

        webpack: commonConfig.webpack,

        webpackMiddleware: commonConfig.webpackMiddleware,

        files: commonConfig.files,

        colors: true,

        autoWatch: false,

        singleRun: true,

        frameworks: ['jasmine'],

        browsers: ['PhantomJS']

    });
};