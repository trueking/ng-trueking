var commonConfig = require('./karma.common.conf');

module.exports = function (config) {
    config.set({

        basePath: commonConfig.basePath,

        preprocessors: {
            'test/**/*.unit.ts': ['webpack'],
            'src/main.ts': ['webpack']
        },

        webpack: commonConfig.webpack,

        webpackMiddleware: commonConfig.webpackMiddleware,

        files: commonConfig.files.concat(['src/main.ts']),

        colors: true,

        autoWatch: true,

        singleRun: false,

        frameworks: ['jasmine'],

        browsers: ['Chrome']

    });
};