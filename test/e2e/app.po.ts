export class AppPage {
    private myFakeText:string;

    constructor() {
        this.myFakeText = 'foo';
    }

    public fakeBtn():string {
        return this.myFakeText;
    }

}