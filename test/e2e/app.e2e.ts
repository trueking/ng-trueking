import * as AppPage from './app.po';

var appPage = new AppPage.AppPage();

describe('Test environment', function() {
    it('should work', function() {
        expect(appPage.fakeBtn()).toBe("foo");
    });
});