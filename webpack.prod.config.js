//Imports
var webpack = require('webpack');
//Webpack plugins
var HtmlWebpackPlugin = require('html-webpack-plugin');
//Common webpack configuration
var commonConfig = require('./webpack.common.config');

var config = {
    entry: {
        app:commonConfig.appEntries,
        vendors: commonConfig.vendors
    },
    output: {
        path: commonConfig.outputPath,
        filename: 'bundle.min.js'
    },
    resolve: commonConfig.resolve,
    plugins: [
        new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.min.js'),
        new webpack.optimize.UglifyJsPlugin({minimize: true}),
        new HtmlWebpackPlugin({
            title: 'Tuerking',
            template: 'src/index.ejs'
        })
    ],
    module: {
        loaders: commonConfig.loaders
    }
};

module.exports = config;