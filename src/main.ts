import * as home from './home';

angular.module('main', ['ui.router']);
export var getModule:() => angular.IModule = () => {
    return angular.module("main");
};

getModule()
    .config(home.HomeConfig);