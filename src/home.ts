let homeTpl = require('./home.tpl.html');
import './home.css';

class HomeCtrl {
    public msg:string;

    constructor() {
        this.msg = "Hi world from typescript!!!";
    }

    public foo() {
        alert(this.msg);
    }
}

export class HomeConfig {

    static $inject = ['$stateProvider'];
    
    constructor($stateProvider:angular.ui.IStateProvider) {

        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: homeTpl,
                controller: HomeCtrl,
                controllerAs: 'ctrl'
            });
    }
}