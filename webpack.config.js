//Imports
var webpack = require('webpack');
//Webpack plugins
var HtmlWebpackPlugin = require('html-webpack-plugin');
//Common webpack configuration
var commonConfig = require('./webpack.common.config');

var config = {
    entry: {
        app: commonConfig.appEntries,
        vendors: commonConfig.vendors
    },
    output: {
        path: commonConfig.outputPath,
        filename: 'bundle.js'
    },
    resolve: commonConfig.resolve,
    plugins: [
        new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js'),
        new HtmlWebpackPlugin({
            title: 'Tuerking',
            template: 'src/index.ejs'
        })
    ],
    devtool: 'source-map',
    module: {
        loaders: commonConfig.loaders
    }
};

module.exports = config;